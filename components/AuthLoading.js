import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native'

export default class AuthLoading extends React.Component {

    componentDidMount() {
        // global.firebase.auth().onAuthStateChanged(user => {
        //     this.props.navigation.navigate(user ? 'Home' : 'Login')
        // })
        this.props.navigation.navigate('Home')
    }


    render() {
        return (
        <View style={styles.container}>
            <Text>Loading</Text>
            <ActivityIndicator size="large" />
        </View>
        )
    }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})