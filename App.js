import React, {useEffect} from 'react';
import SplashScreen from 'react-native-splash-screen';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import AuthLoading from './components/AuthLoading'
import Signup from './layouts/Signup'
import Login from './layouts/Login'
import Home from './layouts/Home'


// This Stack is to be used after Authentication
const AppStack = createStackNavigator(
  { 
    Home: Home
  },
  {
    initialRouteName: 'Home',
  }
 );

// This is an app stack for authentication
const AuthStack = createStackNavigator(
  { 
    Signup: Signup,
    Login: Login 
  },
  {
    initialRouteName: 'Login',
  }
);

// App Stack switch
const AuthStackNav = createSwitchNavigator(
  {
    AuthLoading: AuthLoading,
    App: AppStack,
    Auth: AuthStack
  },
  {
    initialRouteName: 'AuthLoading',
  }
);

const AppContainer = createAppContainer(AuthStackNav);

export default class App extends React.Component {

  componentDidMount(){
    SplashScreen.hide();
  }

  render(){
    return <AppContainer />
  }
}
